import knode from "@_koi/sdk/node.js";
const ktools = new knode.Node();
import fs from "fs";

async function testGenerateWallet() {
  let generatedWallet = await ktools.generateWallet(); //  return True/False
  console.log("generatedWallet  : ", generatedWallet);
  console.log("My wallet: ", ktools.wallet);
  fs.writeFile("data.json", JSON.stringify(ktools.wallet), (err) => {
    if (err) {
      console.log("Error while file writting");
    } else {
      console.log("file writeen successfull!");
    }
  });
  // {kty:"RSA", n:"0vx7agoebGcQSuu...", e:"AQAB"... }
  console.log("My mnemonic: ", ktools.mnemonic);
  // "violin artwork lonely inject resource jewel purity village abstract neglect panda license"
}

testGenerateWallet();
/**
 * wallet - has the key of the generated wallet, which is the private key to the wallet if saved as a .json file.
mnemonic - generates a wallet with mnemonic words

The generated wallet can be accessed via ktools.wallet, Save this info into a file in .JSON format and that's the Private Key to the generated wallet.
The wallet can also be accessed via ktools.mnemonic, If you selected to generate a mnemonic wallet via parameters.
 */
