import knode from "@_koi/sdk/node.js";
const ktools = new knode.Node();
import fs from "fs";

const loadJSON = (path) =>
  JSON.parse(fs.readFileSync(new URL(path, import.meta.url)));

async function testLoadWallet() {
  //  const jwk = await ktools.loadFile("data.json");
  const jwk = loadJSON("data.json");
  console.log("jsk : ", jwk);
  await ktools.loadWallet(jwk);
  console.log("ktools.loadWallet(jwk) : ", await ktools.loadWallet(jwk)); //Loaded wallet with address B4If5_fK6CwPG9sQ8GP5-ZX9KtR14C_wthjiWFctCjk
  console.log("Loaded wallet with address", await ktools.getWalletAddress());
  /**
   * Initialized Koii Tools for true ownership and direct communication using version QA7AIFVx1KBBmzC7WUNhJbDsHlSJArUT0jWrhZMZPS8
Loaded wallet with address B4If5_fK6CwPG9sQ8GP5-ZX9KtR14C_wthjiWFctCjk
   */
}

testLoadWallet();
