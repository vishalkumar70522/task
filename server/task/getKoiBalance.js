import knode from "@_koi/sdk/node.js";
import fs from "fs";
const ktools = new knode.Node();

const loadJSON = (path) =>
  JSON.parse(fs.readFileSync(new URL(path, import.meta.url)));

async function testGetKoiiBalance() {
  const jwk = loadJSON("data.json");
  await ktools.loadWallet(jwk);
  const koiibalance = await ktools.getKoiBalance();
  console.log("KOII balance of given address is ", koiibalance);
}

testGetKoiiBalance();
