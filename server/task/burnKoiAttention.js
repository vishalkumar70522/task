import knode from "@_koi/sdk/node.js";
import fs from "fs";
const ktools = new knode.Node();

const loadJSON = (path) =>
  JSON.parse(fs.readFileSync(new URL(path, import.meta.url)));

async function testburnKoiAttention() {
  const jwk = loadJSON("data.json");
  await ktools.loadWallet(jwk);

  let txId = "rnc8IO7O7fd8Hovvvmq2YZ4y1CI-O0tdRavSGj_meYE";
  var result = await ktools.burnKoiAttention(txId);

  console.log("transaction", result);
}

testburnKoiAttention();
