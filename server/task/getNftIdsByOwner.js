import knode from "@_koi/sdk/node.js";
const ktools = new knode.Node();
async function testGetNftIdsByOwner() {
  try {
    //const ownerAddress = "B4If5_fK6CwPG9sQ8GP5-ZX9KtR14C_wthjiWFctCjk"; // wallet address
    const ownerAddress = "7b4ll1zwenRB8jzyESjFNcRls331buyNl231Pe0V9VI";
    const nfts = await ktools.getNftIdsByOwner(ownerAddress);
    console.log(nfts);
  } catch (error) {
    console.log("error : ", error);
  }
}

testGetNftIdsByOwner();
