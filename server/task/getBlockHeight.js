import knode from "@_koi/sdk/node.js";
const ktools = new knode.Node();

async function testGetBlockHeight() {
  const blockNumber = await ktools.getBlockHeight();
  console.log(blockNumber); // 1099134
}

testGetBlockHeight();
