import { users } from "./data.js";
import express from "express";
import { setup, execute } from "../task.js";

const userRouter = express.Router();

userRouter.get("/all", (req, res) => {
  res.send(users);
});

userRouter.get("/setup", (req, res) => {
  try {
    setup();
    return res.send({
      status: "setup successfull",
    });
  } catch (err) {
    res.send({
      status: "setup unsuccessfull",
      err: err.message,
    });
  }
});

userRouter.get("/execute", (req, res) => {
  execute()
    .then((data) => {
      console.log("execute : ", data);
      return res.send(data);
    })
    .catch((err) =>
      res.send({
        status: "execute unsuccessfull",
        err: err.message,
      })
    );
});
export default userRouter;
