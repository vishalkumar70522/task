import express from "express";
import cors from "cors";
import userRouter from "./router/UserRouter.js";

const app = express();
app.use(express.json({ extended: true }));
app.use(cors({ origin: "*" }));
app.use(express.urlencoded({ extended: true }));

app.use("/user", userRouter);

const port = process.env.PORT || 4000;
const host = process.env.HOST_URL || "http://localhost:";
app.listen(port, async () => {
  console.log(`Server at ${host}${port}`);
});
