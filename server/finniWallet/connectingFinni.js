let isFinnieInjected = false;
window.addEventListener("finnieWalletLoaded", () => {
  isFinnieInjected = true;
});

if (typeof window.ethereum !== "undefined" && window.ethereum.isFinnie) {
  console.log("Finnie is installed");
}
