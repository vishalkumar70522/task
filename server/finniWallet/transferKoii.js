import knode from "@_koi/sdk/node.js";
const ktools = new knode.Node();
import fs from "fs";

const loadJSON = (path) =>
  JSON.parse(fs.readFileSync(new URL(path, import.meta.url)));

async function transferKoii() {
  const jwk = loadJSON("data.json");
  await ktools.loadWallet(jwk);

  const qty = 5;
  const receiverAddress = "azJBB0fI8iKZo3CsWbTOo3bot-uMwUHh__cQ0HzC4Io";
  const transferTxId = await ktools.transfer(qty, receiverAddress, "KOI");
  console.log("Your transaction is " + transferTxId);
}

transferKoii();
//B4If5_fK6CwPG9sQ8GP5-ZX9KtR14C_wthjiWFctCjk