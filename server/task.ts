import cron from "node-cron";
import fs from "fs";
import axios from "axios";
import { Web3Storage, getFilesFromPath } from "web3.storage";
import express from "express";

const NODE_MODE_SERVICE = "service";
const storageClient = new Web3Storage({
  token: process.env.WEB3_STORAGE_KEY,
});

/**
 * @description Setup function is the first  function that is called in executable to setup the node
 */
export async function setup() {
  console.log("Running setup function");
  // Run default setup
  await namespace.defaultTaskSetup();
}

/**In this example, we break up the task into the main task function plus a helper function.
 * The helper function getQuoteOfTheDay calls the quote of the day API and returns the JSON response data. */

// Get quote of the day JSON
async function getQuoteOfTheDay() {
  axios({
    method: "get",
    url: "https://favqs.com/api/qotd",
  })
    .then(async (response) => {
      if (response.data) {
        return response.data;
      }
    })
    .catch((err) => {
      console.log("Failed to get Quote of the day", err.message);
    });
}
/**
 * The task function gets the quote data from the helper function and then signs and writes that data to IPFS using Web3.Storage.
 * It then takes the CID of the data returned and submits it to K2.
 */
async function task() {
  const quoteOfTheDay = getQuoteOfTheDay();
  const signedJSON = await namespace.signData(qodJSON);
  fs.writeFileSync("qod.json", signedJSON);
  if (storageClient) {
    // Storing on IPFS through web3 storage as example
    const file = await getFilesFromPath("./qod.json");
    const cid = await storageClient.put(file);
    console.log("CID of Uploaded Data: ", cid);
    await namespace.redisSet("cid", cid);
    await namespace.checkSubmissionAndUpdateRound(cid);
  } else {
    console.error("No web3 storage API key provided");
  }
}

/**
 * @description Using to validate an individual node. Returns true if node is valid
 */
async function validateNode(node) {
  console.log("Validating Node", node);
  const cid = node.submission_value;
  const res = await client.get(cid);
  const { data, error } = await namespace.verifySignedData(
    res,
    node.submitterPubkey
  );
  const quoteOfTheDay = getQuoteOfTheDay();
  // If
  //  CID does not point to hosted data
  //  Or data is signed incorrectly
  //  Or if that data does not match the quote of the day
  //  -> Then the node is invalid
  if (!res.ok || error || data != quoteOfTheDay) {
    return false;
  } else {
    return true;
  }
}

/**
 * The executable function will be run after your setup function has finished. It must return an array of cron jobs.
 * In this example, we define the two required cron jobs: the task job and the validate and vote job.
 *  The task job submits the task results to K2 and validate and vote, validates each node and submits votes to K2.
 */

/**
 * @description Execute function is called just after the setup function to run Submit, Vote API in cron job
 * @returns return all cron schedule from execute function for graceful termination
 */
export async function execute() {
  console.log("Running execute function");
  console.log("NODE MODE", process.env.NODE_MODE);

  let cronArray = [];
  if (process.env.NODE_MODE == NODE_MODE_SERVICE) {
    cronArray.push(cron.schedule("*/1 * * * *", task));
  }
  cronArray.push(
    cron.schedule("*/1 * * * *", () => {
      namespace.validateAndVoteOnNodes(validateNode);
    })
  );

  return cronArray;
}
